package cn.buildworld.netty.server.start.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author MiChong
 * @date 2021-09-06 14:24
 */
@Configuration
@EnableConfigurationProperties(NettyProperties.class)
//当类路径下有指定的类为true
@ConditionalOnClass(NettyServer.class)
@ConditionalOnProperty(prefix = "netty.server", value = "enabled", matchIfMissing = true)
public class NettyServerAutoConfiguration {

    @Autowired
    private NettyProperties nettyProperties;

    @Bean
    @ConditionalOnMissingBean(NettyServer.class)
    public NettyServer nettyServer() {
        NettyServer nettyServer = new NettyServer(nettyProperties);
        return nettyServer;
    }

}
