package cn.buildworld.netty.client.web.controller;


import cn.buildworld.netty.start.client.common.ResponseResult;
import cn.buildworld.netty.start.client.config.NettyClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author MiChong
 * @date 2021-09-06 10:01
 */
@RestController
@Slf4j
public class UserController {

    @Autowired
    private NettyClientUtil nettyClientUtil;

    @PostMapping("/helloNetty")
    public ResponseResult helloNetty(@RequestParam String msg) {
        return nettyClientUtil.sendMessage(msg);
    }
}