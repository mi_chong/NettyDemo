package cn.buildworld.netty.webstart;

import cn.buildworld.netty.server.start.config.NettyServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author MiChong
 * @date 2021-08-31 10:35
 */
@Slf4j
@SpringBootApplication
public class WebStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebStartApplication.class, args);
    }

}
