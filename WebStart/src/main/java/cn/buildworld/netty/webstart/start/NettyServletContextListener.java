package cn.buildworld.netty.webstart.start;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author MiChong
 * @date 2021-09-06 16:47
 */
public class NettyServletContextListener implements ServletContextListener {

    /**
     * 在初始化Web应用程序中的任何过滤器或servlet之前，将通知所有servletContextListener上下文初始化。
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("执行contextInitialized方法");
    }
}
