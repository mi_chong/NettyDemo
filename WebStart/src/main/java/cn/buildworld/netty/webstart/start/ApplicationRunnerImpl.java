package cn.buildworld.netty.webstart.start;

import cn.buildworld.netty.server.start.config.NettyServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author MiChong
 * @date 2021-09-06 15:03
 */
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Autowired
    private NettyServer nettyServer;

    /**
     * 用于指示bean包含在SpringApplication中时应运行的接口。可以定义多个applicationrunner bean
     * 在同一应用程序上下文中，可以使用有序接口或@order注释对其进行排序。
     */

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("通过实现ApplicationRunner接口，在spring boot项目启动后打印参数");
         // nettyServer.start();
    }
}
