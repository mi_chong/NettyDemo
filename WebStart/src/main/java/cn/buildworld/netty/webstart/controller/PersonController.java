package cn.buildworld.netty.webstart.controller;

import cn.buildworld.netty.start.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author MiChong
 * @date 2021-08-31 10:37
 */
@RestController
public class PersonController {
    @Autowired
    private PersonService personService;

    @GetMapping("/person")
    public String getPerson(){
        personService.sayHello();
        return "ok";
    }
}
