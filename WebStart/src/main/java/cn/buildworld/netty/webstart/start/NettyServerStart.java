package cn.buildworld.netty.webstart.start;

import cn.buildworld.netty.server.start.config.NettyServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 将要执行的方法所在的类交个spring容器扫描(@Component),并且在要执行的方法上添加@PostConstruct注解或者静态代码块执行
 *
 * @author MiChong
 * @date 2021-09-06 15:38
 */
@Component
public class NettyServerStart {
    @Autowired
    private NettyServer nettyServer;

    @PostConstruct
    public void start() {
        System.out.println(" 使用@PostConstruct注解 ");
        new Thread(() -> {
            nettyServer.start();
        }).start();
    }
}
