package cn.buildworld.netty.webstart.start;

import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * 实现ServletContextAware接口并重写其setServletContext方法
 *
 * @author MiChong
 * @date 2021-09-06 16:46
 */
@Component
public class NettyServletContextAware implements ServletContextAware {
    /**
     * 在填充普通bean属性之后但在初始化之前调用,注意：该方法会在填充完普通Bean的属性，但是还没有进行Bean的初始化之前执行
     * 类似于initializingbean的afterpropertiesset或自定义init方法的回调
     */
    @Override
    public void setServletContext(ServletContext servletContext) {
        System.out.println("setServletContext方法");
    }
}
