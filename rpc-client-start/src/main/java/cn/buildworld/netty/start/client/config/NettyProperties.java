package cn.buildworld.netty.start.client.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.Serializable;

/**
 * Netty属性配置类
 *
 * @author MiChong
 * @date 2021-09-06 13:51
 */

@SuppressWarnings("serial")
@ConfigurationProperties(prefix = "netty.connection")
public class NettyProperties implements Serializable {
    // 地址
    private String hostname = "127.0.0.1";
    // 端口
    private Integer host = 8082;
    // 主线程数目
    private Integer bossGroupCounts = 1;
    // 工作线程数目
    private Integer workGroupCounts = 200;

    public NettyProperties() {

    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getHost() {
        return host;
    }

    public void setHost(Integer host) {
        this.host = host;
    }

    public Integer getBossGroupCounts() {
        return bossGroupCounts;
    }

    public void setBossGroupCounts(Integer bossGroupCounts) {
        this.bossGroupCounts = bossGroupCounts;
    }

    public Integer getWorkGroupCounts() {
        return workGroupCounts;
    }

    public void setWorkGroupCounts(Integer workGroupCounts) {
        this.workGroupCounts = workGroupCounts;
    }
}
