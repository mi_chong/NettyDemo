package cn.buildworld.netty.start.client.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author MiChong
 * @date 2021-09-06 14:24
 */
@Configuration
@EnableConfigurationProperties(NettyProperties.class)
//当类路径下有指定的类为true
@ConditionalOnClass(NettyClientUtil.class)
@ConditionalOnProperty(prefix = "netty.connection", value = "enabled", matchIfMissing = true)
public class NettyClientrAutoConfiguration {

    @Autowired
    private NettyProperties nettyProperties;

    @Bean
    @ConditionalOnMissingBean(NettyClientUtil.class)
    public NettyClientUtil nettyServer() {
        NettyClientUtil nettyServer = new NettyClientUtil(nettyProperties);
        return nettyServer;
    }
}
